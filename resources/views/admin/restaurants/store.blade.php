@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Inserindo um Restaurante</h1>
    <hr>
    <form action="{{route('restaurant.store')}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <p class="form-group">
            <label>Nome do Restaurante</label>
            <input type="text" name="name" value="{{old('name')}}" class="form-control">
            @if($errors->has('name'))
                {{$errors->first('name')}}
            @endif
        </p>

        <p>
            <label>Endereço</label>
            <input type="text" name="address" value="{{old('address')}}" class="form-control">
            @if($errors->has('address'))
                {{$errors->first('address')}}
            @endif
        </p>

        <p>
            <label>Fale sobre o restaurante</label>
            <textarea type="text" name="description" cols="30" rows="10" class="form-control">{{old('description')}}</textarea>
            @if($errors->has('description'))
                {{$errors->first('description')}}
            @endif
        </p>

        <input type="submit" value="Cadastrar" class="btn btn-primary">
    </form>
</div>
@endsection