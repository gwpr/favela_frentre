@extends('layouts.app')
@section('content')
<div class="container">
    <a href="{{route('restaurant.new')}}" class="float-right btn btn-success">Novo</a>
    <h1>Restaurantes</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Criado Em</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        @foreach($restaurants as $r)
            <tr>
                <td>{{$r->id}}</td>
                <td>{{$r->name}}</td>
                <td>{{$r->created_at}}</td>
                <td>
                    <a href="{{route('restaurant.edit', ['restaurant'=>$r])}}" class="btn btn-primary">Editar</a>
                </td>
                <td>
                    <a href="{{route('restaurant.remove', ['restaurant'=>$r->id])}}" class="btn btn-danger">Excluir</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
    @endsection
