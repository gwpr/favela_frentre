@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Editando um Restaurante</h1>
    <hr>

    <form action="{{route('restaurant.update',['restaurant'=>$restaurant->id])}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <p>
            <label>Nome do Restaurante</label>
            <input type="text" name="name" value="{{$restaurant->name}}">
            @if($errors->has('name'))
                {{$errors->first('name')}}
            @endif
        </p>

        <p>
            <label>Endereço</label>
            <input type="text" name="address" value="{{$restaurant->address}}">
            @if($errors->has('address'))
                {{$errors->first('address')}}
            @endif
        </p>

        <p>
            <label>Fale sobre o restaurante</label>
            <textarea type="text" name="description" cols="30" rows="10">{{$restaurant->description}}</textarea>
            @if($errors->has('description'))
                {{$errors->first('description')}}
            @endif
        </p>

        <input type="submit" value="Atualizar">
    </form>
</div>
@endsection