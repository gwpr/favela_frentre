<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcessoUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acesso_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nivel');
            $table->integer('consultar')->unsigned();
            $table->integer('incluir')->unsigned();
            $table->integer('editar')->unsigned();
            $table->integer('excluir')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acessos');
    }
}
