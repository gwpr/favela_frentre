<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return $users;
    }

    public function new()
    {
        return view('admin.users.store');
    }

    public function store(Request $request)
    {
       $userData = $request->all();
       $user = new User();
       $user->create($userData);

       print 'Usuario Criado';
    }

    public function edit(Request $request,$id)
    {
        $userData = $request->all();

        $user= User::findOrFail($id);

        return view('admin.users.edit', compact('restaurant'));

    }

    public function update(Request $request,$id)
    {
        $userData = $request->all();

        $user=User::findOrFail($id);
        $user->update($userData);

        $user = User::all();
        return view('admin.users.index',compact('users'));
    }

    public function remove(Request $request,$id)
    {
        $userData = $request->all();

        $user=User::findOrFail($id);
        $user->delete();

        $user = User::all();
        return view('admin.users.index',compact('users'));
    }
}
