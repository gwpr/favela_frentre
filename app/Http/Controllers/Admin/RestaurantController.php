<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RestauranteRequest;
use App\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RestaurantController extends Controller
{
    public function index()
    {
        $restaurants = Restaurant::all();
        return view('admin.restaurants.index',compact('restaurants'));
    }

    public function new()
    {
        return view('admin.restaurants.store');
    }

    public function store(RestauranteRequest $request)
    {
       $restauranteData = $request->all();

       $restaurante=new Restaurant();
       $restaurante->create($restauranteData);

       print 'Restaurante Criado';
    }

    public function edit(Request $request,$id)
    {
        $restauranteData = $request->all();

        $restaurant=Restaurant::findOrFail($id);

        return view('admin.restaurants.edit', compact('restaurant'));

    }

    public function update(RestauranteRequest $request,$id)
    {
        $restauranteData = $request->all();

        $restaurante=Restaurant::findOrFail($id);
        $restaurante->update($restauranteData);

        $restaurants = Restaurant::all();
        return view('admin.restaurants.index',compact('restaurants'));
    }

    public function remove(Request $request,$id)
    {
        $restauranteData = $request->all();

        $restaurante=Restaurant::findOrFail($id);
        $restaurante->delete();

        $restaurants = Restaurant::all();
        return view('admin.restaurants.index',compact('restaurants'));
    }
}
