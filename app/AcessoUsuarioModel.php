<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcessoUsuarioModel extends Model
{
   protected $fillable =[
        'id',
       'nivel',
       'consultar',
       'incluir',
       'editar',
       'excluir'
   ];
}
